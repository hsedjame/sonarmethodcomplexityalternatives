package pack;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public class ConditionedActions<T, S> {

    private Predicate<T> predicat;
    private Function<T, S> positiveAction;
    private Function<T, S> negativeAction;
    private boolean breakIfPredicatTest;

    private ConditionedActions() {
    }

    public ConditionedActions(Predicate<T> predicat, Function<T, S> positiveAction) {
        this(predicat, positiveAction, null, false);
    }

    public ConditionedActions(Predicate<T> predicat, Function<T, S> positiveAction, boolean breakIfPredicatTest) {
        this(predicat, positiveAction, null, breakIfPredicatTest);
    }

    public ConditionedActions(Predicate<T> predicat, Function<T, S> positiveAction, Function<T, S> negativeAction, boolean breakIfPredicatTest) {

        Objects.requireNonNull(predicat, "Predicat should not be null.");
        Objects.requireNonNull(positiveAction, "PositiveAction should not be null.");

        this.predicat = predicat;
        this.positiveAction = positiveAction;
        this.negativeAction = negativeAction;
        this.breakIfPredicatTest = breakIfPredicatTest;
    }

    public Predicate<T> getPredicat() {
        return predicat;
    }

    public Function<T, S> getPositiveAction() {
        return positiveAction;
    }

    public Function<T, S> getNegativeAction() {
        return negativeAction;
    }

    public boolean isBreakIfPredicatTest() {
        return breakIfPredicatTest;
    }

    public Optional<S> execute(T t) {

        if (this.predicat.test(t)) {
            return Optional.of(this.apply(t));
        }

        return Objects.nonNull(this.negativeAction) ? Optional.of(this.negativeAction.apply(t)) : Optional.empty();
    }

    public S apply(T t) {
        return this.positiveAction.apply(t);
    }

}
