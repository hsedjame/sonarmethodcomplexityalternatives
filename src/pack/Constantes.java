package pack;

final class Constantes {

    static final String STEP_1 = "step1";
    static final String STEP_2 = "step2";
    static final String STEP_3 = "step3";
    static final String STEP_4 = "step4";
    static final String STEP_5 = "step5";
    static final String STEP_6 = "step6";
    static final String STEP_7 = "step7";
    static final String STEP_8 = "step8";
    static final String STEP_9 = "step9";
    static final String STEP_10 = "step10";
    static final String STEP_11 = "step11";
    static final String STEP_12 = "step12";
    static final String STEP_13 = "step13";
    static final String STEP_14 = "step14";
    static final String STEP_15 = "step15";
    static final String STEP_16 = "step16";
    static final String STEP_17 = "step17";
    static final String STEP_18 = "step18";
    static final String STEP_19 = "step19";
    static final String STEP_20 = "step20";

    private Constantes() { }
}
