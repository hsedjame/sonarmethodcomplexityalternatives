package pack;

import java.util.Objects;
import java.util.function.Consumer;

public class StepDeterminer {

    private Consumer<BaseObject> step1 = object -> object.setStep("1");
    private Consumer<BaseObject> step2 = object -> object.setStep("2");
    private Consumer<BaseObject> step3 = object -> object.setStep("3");
    private Consumer<BaseObject> step4 = object -> object.setStep("4");
    private Consumer<BaseObject> step5 = object -> object.setStep("5");
    private Consumer<BaseObject> step6 = object -> object.setStep("6");
    private Consumer<BaseObject> step7 = object -> object.setStep("7");
    private Consumer<BaseObject> step8 = object -> object.setStep("8");
    private Consumer<BaseObject> step9 = object -> object.setStep("9");
    private Consumer<BaseObject> step10 = object -> object.setStep("10");
    private Consumer<BaseObject> step11 = object -> object.setStep("11");
    private Consumer<BaseObject> step12 = object -> object.setStep("12");
    private Consumer<BaseObject> step13 = object -> object.setStep("13");
    private Consumer<BaseObject> step14 = object -> object.setStep("14");
    private Consumer<BaseObject> step15 = object -> object.setStep("15");
    private Consumer<BaseObject> step16 = object -> object.setStep("16");
    private Consumer<BaseObject> step17 = object -> object.setStep("17");
    private Consumer<BaseObject> step18 = object -> object.setStep("18");
    private Consumer<BaseObject> step19 = object -> object.setStep("19");
    private Consumer<BaseObject> step20 = object -> object.setStep("20");

    private static StepDeterminer instance;

    private StepDeterminer() {
    }

    public static StepDeterminer getInstance() {
        if (Objects.isNull(instance)) instance = new StepDeterminer();
        return instance;
    }

    public Consumer<BaseObject> getStep1() {
        return step1;
    }

    public Consumer<BaseObject> getStep2() {
        return step2;
    }

    public Consumer<BaseObject> getStep3() {
        return step3;
    }

    public Consumer<BaseObject> getStep4() {
        return step4;
    }

    public Consumer<BaseObject> getStep5() {
        return step5;
    }

    public Consumer<BaseObject> getStep6() {
        return step6;
    }

    public Consumer<BaseObject> getStep7() {
        return step7;
    }

    public Consumer<BaseObject> getStep8() {
        return step8;
    }

    public Consumer<BaseObject> getStep9() {
        return step9;
    }

    public Consumer<BaseObject> getStep10() {
        return step10;
    }

    public Consumer<BaseObject> getStep11() {
        return step11;
    }

    public Consumer<BaseObject> getStep12() {
        return step12;
    }

    public Consumer<BaseObject> getStep13() {
        return step13;
    }

    public Consumer<BaseObject> getStep14() {
        return step14;
    }

    public Consumer<BaseObject> getStep15() {
        return step15;
    }

    public Consumer<BaseObject> getStep16() {
        return step16;
    }

    public Consumer<BaseObject> getStep17() {
        return step17;
    }

    public Consumer<BaseObject> getStep18() {
        return step18;
    }

    public Consumer<BaseObject> getStep19() {
        return step19;
    }

    public Consumer<BaseObject> getStep20() {
        return step20;
    }


}
