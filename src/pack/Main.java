package pack;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class Main {

    private static final RegleMetiers regleMetiers = RegleMetiers.getInstance();

    private static final Logger LOGGER = Logger.getLogger("Main");

    public static void main(String[] args) {

        BaseObject baseObject = new BaseObject();

        baseObject.setConnected(true);

        determineStepWithComplexity(baseObject, "step2");

        determineStepWithoutComplexity(baseObject, "step3");

        List<String> instructions = provideInstructionsWithComplexity(baseObject);

        List<String> instructions2 = provideInstructionsWithoutComplexity(baseObject);

        instructions.forEach(LOGGER::info);
        instructions2.forEach(LOGGER::info);
    }

    private static void determineStepWithComplexity(BaseObject object, String step) {

        switch (step) {
            case Constantes.STEP_1:
                object.setStep("1");
                break;
            case Constantes.STEP_2:
                object.setStep("2");
                break;
            case Constantes.STEP_3:
                object.setStep("3");
                break;
            case Constantes.STEP_4:
                object.setStep("4");
                break;
            case Constantes.STEP_5:
                object.setStep("5");
                break;
            case Constantes.STEP_6:
                object.setStep("6");
                break;
            case Constantes.STEP_7:
                object.setStep("7");
                break;
            case Constantes.STEP_8:
                object.setStep("8");
                break;
            case Constantes.STEP_9:
                object.setStep("9");
                break;
            case Constantes.STEP_10:
                object.setStep("10");
                break;
            case Constantes.STEP_11:
                object.setStep("11");
                break;
            case Constantes.STEP_12:
                object.setStep("12");
                break;
            case Constantes.STEP_13:
                object.setStep("13");
                break;
            case Constantes.STEP_14:
                object.setStep("14");
                break;
            case Constantes.STEP_15:
                object.setStep("15");
                break;
            case Constantes.STEP_16:
                object.setStep("16");
                break;
            case Constantes.STEP_17:
                object.setStep("5");
                break;
            case Constantes.STEP_18:
                object.setStep("17");
                break;
            case Constantes.STEP_19:
                object.setStep("18");
                break;
            case Constantes.STEP_20:
                object.setStep("19");
                break;
            default:
                object.setStep("default");
        }
    }

    private static void determineStepWithoutComplexity(BaseObject object, String step) {

        try {

            Method getter = StepDeterminer.class.getMethod(getGetterName(step));

            Consumer<BaseObject> consumer = (Consumer<BaseObject>) getter.invoke(StepDeterminer.getInstance());


            if (Objects.nonNull(consumer)) {
                consumer.accept(object);
            }

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            LOGGER.info(e.getMessage());
        }

    }

    private static String getGetterName(String step) {
        String firstChar = String.valueOf(step.charAt(0));
        return "get" + step.replace(firstChar, firstChar.toUpperCase());
    }

    private static List<String> provideInstructionsWithComplexity(BaseObject object) {

        List<String> instructions = new ArrayList<>();

        if(object.isConnected()) {
            instructions.add("Connection Ok");
        } else if (!object.isConnected()){
            instructions.add("Please connect to continue");
        } else if (Objects.nonNull(object.getStep())) {
            instructions.add("Current Step : " + object.getStep());
            instructions.add("Next Step ...");
        } else {
            object.setStep("step1");
            instructions.add("Current Step : " + object.getStep());
            instructions.add("Next Step ...");
        }

        return instructions;
    }

    private static List<String> provideInstructionsWithoutComplexity(BaseObject object) {

        List<String> instructions = new ArrayList<>();

        Collection<ConditionedActions<BaseObject, List<String>>> regles = regleMetiers.getRegles();

        for (ConditionedActions<BaseObject, List<String>> action: regles) {
            action.execute(object).ifPresent(instructions::addAll);
            if (action.isBreakIfPredicatTest()) {
                break;
            }
        }

        return instructions;

    }
}
