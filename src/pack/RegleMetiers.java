package pack;

import java.util.*;

public final class RegleMetiers {

    private Collection<ConditionedActions<BaseObject, List<String>>> regles = new ArrayList<>();

    private static RegleMetiers instance;

    private RegleMetiers() {

        /* Définition des règles métiers */

        ConditionedActions<BaseObject, List<String>> connection = new ConditionedActions<>(
                BaseObject::isConnected,
                o -> Collections.singletonList("Connection Ok"),
                o -> Collections.singletonList("Please connect to continue"),
                false
        );

        ConditionedActions<BaseObject, List<String>> stepNotNull = new ConditionedActions<>(
                o -> Objects.nonNull(o.getStep()),
                o -> Arrays.asList("Current Step : " + o.getStep(), "Next Step ..."),
                o -> {
                    o.setStep("step1");
                    return Arrays.asList("Current Step : " + o.getStep(), "Next Step ...");
                },
                true

        );

        regles.addAll(Arrays.asList(connection, stepNotNull));
    }

    public static RegleMetiers getInstance() {

        if (Objects.isNull(instance)){
            instance = new RegleMetiers();
        }

        return instance;
    }

    public Collection<ConditionedActions<BaseObject, List<String>>> getRegles() {
        return regles;
    }
}
